var http = require('http');
var url = require('url');
var items = [];

http.createServer(function(req, res) {

	switch(req.method) {
		case 'POST':

			var item = '';
			
			req.setEncoding('utf8');
			
			req.on('data', function(chunk) {
				item += chunk;
				console.log(chunk.toString());
			});
			
			req.on('end', function() {
				items.push(item);
				res.end('OK \n');
			});
		break;
				
		case 'GET':
		
			items.forEach(function(item, index) {
				res.write(index + ')' + item + '\n');
			});
				
			res.end();
		break;
		
		case 'DELETE':
		
			var i = url.parse(req.url).path.slice(1);
			var item = parseInt(i);
			
			if(!items[i]) {
				res.statusCode = 404;
				res.end('Item not found');
			}
			
			else {
				items.splice(i, 1);
				res.end('Done');
			}
		break;
		
		case 'PUT':
		
			var item = '';
			var i = url.parse(req.url).path.slice(1);
			var updatedItem = parseInt(i);
			
			req.setEncoding('utf8');
		
			req.on('data', function(chunk) {
				item += chunk;
				console.log(chunk.toString());
			});
			
			req.on('end', function() {
				items[updatedItem] = item;
				res.end('OK \n');
			});
		
		break;
	}
	
}).listen(9090);